# These are my configs for docker

## Guacamole
After bring up the [guacamole stack](guacamole.yaml), you need to configure the DB.

Don't forget to create the volume:
```
$ docker volume create guacdb_data
```

Generate the install script and copy it to the DB container
```
$ docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --postgres > initdb.sql
$ docker cp ./initdb.sql guacdb:/
$ rm ./initdb.sql
```

Create the database and configure
```
$ docker exec -it guacdb bash
$ createdb -U postgres guacamole
$ cat initdb.sql | psql -U postgres -d guacamole_db -f -
$ psql -U postgres -d guacamole
# CREATE USER guacamole WITH PASSWORD 'Quality-Passphrase-Here1';
# GRANT SELECT,INSERT,UPDATE,DELETE ON ALL TABLES IN SCHEMA public TO guacamole;
# GRANT SELECT,USAGE ON ALL SEQUENCES IN SCHEMA public TO guacamole;
# \q
```

That should be it.  Access http://yourserver.lan:8083/guacamole, and login with `guacadmin`:`guacadmin` (be sure to change that :P)

### References
* https://hub.docker.com/r/guacamole/guacamole
* https://guacamole.incubator.apache.org/doc/gug/jdbc-auth.html#jdbc-auth-postgresql
